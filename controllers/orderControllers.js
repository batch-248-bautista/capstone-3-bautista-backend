//use  Models
const Orders = require("../models/Orders");
const Products = require("../models/Products");
const User = require("../models/User");


const bcrypt = require("bcrypt");
const auth = require("../auth");

//non-admin create an order
module.exports.order = (data) => {

	let newOrder = new Orders ({
		userId : data.userId,
        productId: data.productId
	});
	return newOrder.save().then((order, error)=>{
		if (error){
			return false
		}
		else{
			return order;
		}
	})
}

/*
	Add to cart:
	Request: 
		productId, quantity - body
		orderId - params
*/
/*module.exports.addToCart = async (data) => {
    try {
        let order = await Orders.findById(data.orderId);
        let product = await Products.findById(data.productId);
        let price = product.price;
        let quantity = data.quantity;

        if (order !== null && product !== null) {
            order.products.push({ productId: data.productId, quantity: quantity });
            product.orders.push({ orderId: data.orderId, quantity: quantity });

            let isOrderUpdated = await order.save().then((order, error) => {
                if (error) {
                    return false;
                } else {
                    return true;
                }
            });

            let isProductUpdated = await product.save().then((product, error) => {
                if (error) {
                    return false;
                } else {
                    return true;
                }
            });

            if (isOrderUpdated && isProductUpdated) {
                //total amount calculation
                let promises = order.products.map(async (product) => {
                    let productPrice =
                        product.productId === data.productId
                            ? price
                            : (await Products.findById(product.productId)).price;
                    return productPrice * product.quantity;
                });

                let productPrices = await Promise.all(promises);
                let totalAmount = productPrices.reduce((total, price) => total + price, 0);

                order.totalAmount = totalAmount;
                await order.save();
                return { success: true, totalAmount: totalAmount };
            } else {
                return { success: false };
            }
        } else {
            return { success: false, message: "Order or product not found" };
        }
    } catch (error) {
        console.log(error);
        return { success: false, message: error.message };
    }
};*/
module.exports.addToCart = async (data) => {
  try {
    let order = await Orders.findById(data.orderId);

    if (order === null) {
      // If order doesn't exist, create a new one
      order = await Orders.create({
        _id: data.orderId,
        userId: data.userId, // add user ID here
        products: [],
        totalAmount: 0
      });
    }
    else {
      order.user = data.userId;
    }

    let product = await Products.findById(data.productId);
    let price = product.price;
    let quantity = data.quantity;

    order.products.push({ productId: data.productId, quantity: quantity, userId: data.userId });
    product.orders.push({ orderId: data.orderId, quantity: quantity });

    let isOrderUpdated = await order.save().then((order, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });

    let isProductUpdated = await product.save().then((product, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });

    if (isOrderUpdated && isProductUpdated) {
      // Calculate updated products with subtotal
      let updatedProducts = await Promise.all(
        order.products.map(async (product) => {
          let productPrice =
            product.productId === data.productId
              ? price
              : (await Products.findById(product.productId)).price;
          let subtotal = productPrice * product.quantity;
          return { productId: product.productId, quantity: product.quantity, subtotal: subtotal };
        })
      );

      // Calculate total amount
      let totalAmount = updatedProducts.reduce((total, product) => total + product.subtotal, 0);

      order.products = updatedProducts;
      order.totalAmount = totalAmount;
      order.user = data.userId;

      await order.save(); // save the updated order to the serverawait order.save();
      return { success: true, userId: data.userId, totalAmount: totalAmount,  updatedProducts: updatedProducts };
    }

    return { success: false, message: "Order or product not found" };
  } catch (error) {
    console.log(error);
    return { success: false, message: error.message };
  }
};

//add product from the specific orderId
module.exports.addProductToCart = async (data) => {
  try {
    let order = await Orders.findById(data.orderId);

    if (order === null) {
      // If order doesn't exist, create a new one
      order = await Orders.create({
        _id: data.orderId,
        userId: data.userId, // add user ID here
        products: [],
        totalAmount: 0
      });
    }
    else {
      order.user = data.userId;
    }

    let product = await Products.findById(data.productId);
    
    let price = product.price;
    let quantity = data.quantity;

    order.products.push({ productId: data.productId, quantity: quantity, userId: data.userId });
    product.orders.push({ orderId: data.orderId, quantity: quantity });

    let isOrderUpdated = await order.save().then((order, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });

    let isProductUpdated = await product.save().then((product, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    });

    if (isOrderUpdated && isProductUpdated) {
      // Calculate updated products with subtotal
      let updatedProducts = await Promise.all(
        order.products.map(async (product) => {
          let productPrice =
            product.productId === data.productId
              ? price
              : (await Products.findById(product.productId)).price;
          let subtotal = productPrice * product.quantity;
          return { productId: product.productId, quantity: product.quantity, subtotal: subtotal };
        })
      );

      // Calculate total amount
      let totalAmount = updatedProducts.reduce((total, product) => total + product.subtotal, 0);

      order.products = updatedProducts;
      order.totalAmount = totalAmount;
      order.user = data.userId;

      await order.save(); // save the updated order to the serverawait order.save();
      return { success: true, userId: data.userId, totalAmount: totalAmount,  updatedProducts: updatedProducts };
    }

    return { success: false, message: "Order or product not found" };
  } catch (error) {
    console.log(error);
    return { success: false, message: error.message };
  }
};

//retrieve authenticated users's orders
module.exports.getOrdersByUserId = (userId) => {
  return Orders.find({ userId: userId })
    .then(result => {
      return result;
    })
    .catch(error => {
      throw new Error(error);
    });
};



//retriev single order
module.exports.getOrder = (data) =>{
  return Orders.findById(data.orderId).then(result => {
    return result;
  })
}

module.exports.getAllOrders =()=>{
  return Orders.find({}).then(result => {
    return result;
  })
}







module.exports.pendingOrders =(reqBody)=>{
    return Orders.findById(reqBody.orderId)
    .then((result, error)=>{
      if (error){
        return `Not Found.`;
      }
        result.isPaid = false;
        return result.save();
        })
    }

module.exports.paidOrders =(reqBody)=>{
    return Orders.findById(reqBody.orderId)
    .then((result, error)=>{
      if (error){
        return `Not Found.`;
      }
        result.isPaid = true;
        return result.save();
        })
    }
//change product quantity in orders
//orderId in params, then find products.objectid

/*module.exports.updateOrderProduct = async (data) => {
  try {
    const order = await Orders.findById(data.orderId);
    const product = await Products.findById(data.productId);
    const price = product.price;
    const quantity = data.quantity;

    if (!order || !product) {
      return { success: false, message: "Order or product not found" };
    }

    // Find the index of the product in the order's products array
    const productIndex = order.products.findIndex(p => p.productId === data.productId);
    if (productIndex === -1) {
      // Product not found in order's products array
      return { success: false, message: "Product not found in order" };
    }

    // Update the quantity of the product in the order's products array
    order.products[productIndex].quantity = quantity;

    // Calculate the subtotal amount of the accessed product
    const subTotal = price * quantity;
    
    // Update the subtotal amount of the product in the order's products array
    order.products[productIndex].subTotal = subTotal;

    // Update the total amount of the order
    const total = order.products.reduce((acc, product) => acc + product.subTotal, 0);
    order.total = total;

    // Save the updated order to the database
    const isOrderUpdated = await order.save();

    if (isOrderUpdated) {
      return { success: true, subTotal: subTotal };
    } else {
      return { success: false };
    }
  } catch (error) {
    console.log(error);
    return { success: false, message: error.message };
  }
};
*/
module.exports.updateOrderProduct = async (data) => {
  try {
    const order = await Orders.findById(data.orderId);
    const product = await Products.findById(data.productId);
    const price = product.price;
    const quantity = data.quantity;

    if (!order || !product) {
      return { success: false, message: "Order or product not found" };
    }

    // Find the index of the product in the order's products array
    const productIndex = order.products.findIndex(p => p.productId === data.productId);
    if (productIndex === -1) {
      // Product not found in order's products array
      return { success: false, message: "Product not found in order" };
    }

    // Update the quantity of the product in the order's products array
    order.products[productIndex].quantity = quantity;

    // Calculate the subtotal amount of the accessed product
    const subtotal = price * quantity;
    
    // Update the subtotal amount of the product in the order's products array
    order.products[productIndex].subtotal = subtotal;

    // Update the total amount of the order
    order.totalAmount = order.products.reduce((total, product) => total + product.subtotal, 0);

    // Save the updated order to the database
    const isOrderUpdated = await order.save();

    if (isOrderUpdated) {
      return { success: true, subtotal: subtotal, totalAmount: order.totalAmount };
    } else {
      return { success: false };
    }
  } catch (error) {
    console.log(error);
    return { success: false, message: error.message };
  }
};

//retrieve added products
module.exports.addedProducts = (data)=>{
	return Orders.find(data.products).then(result => {
		return result;
	})
}

//remove product in the order

module.exports.removeOrderProduct = async ({ orderId, productId }) => {
  try {
    const order = await Orders.findById(orderId);
    const product = await Products.findById(productId);

    console.log('orderId:', orderId);
    console.log('productId:', productId);
    if (!order || !product) {
      throw new Error('Order or product not found');
    }

    // Find the index of the product in the order's products array
    const productIndex = order.products.findIndex(({ productId: productId }) => productId === productId);

    if (productIndex === -1) {
      // Product not found in order's products array
      return { success: false, message: 'Product not found in order' };
    }

    // Remove the product from the order's products array
    order.products.splice(productIndex, 1);

    // Remove the order from the product's orders array
    const orderIndex = product.orders.findIndex(({ orderId: orderId }) => orderId === orderId);
    if (orderIndex !== -1) {
      product.orders.splice(orderIndex, 1);
    }

    // Calculate the new total amount of the order
    const totalAmount = order.products.reduce((total, { price, quantity }) => {
      return total + price * quantity;
    }, 0);

    // Update the total amount of the order
    order.totalAmount = isNaN(totalAmount) ? 0 : totalAmount;

    // Save the updated order and product to the database
    await order.save();
    await product.save();

    return `Product successfully removed.`;
  } catch (error) {
    console.log(error);
    throw new Error(error.message);
  }
};



// Delete an order by a specific orderId
exports.deleteOrder = (req, res) => {
  const decodedToken = auth.decode(req.headers.authorization);
  Orders.findById(req.params.orderId)
    .then(order => {
      if (decodedToken.id !== order.userId) {
        res.send('Unauthorized.');
      } else {
        Orders.findByIdAndDelete(req.params.orderId)
          .then(result => res.send({ success: true, message: 'Order deleted successfully.' }))
          .catch(error => {
            console.log(error);
            res.send({ success: false, message: error.message });
          });
      }
    })
    .catch(error => {
      console.log(error);
      res.send({ success: false, message: error.message });
    });
};

