//use User Model
const User = require("../models/User");
/*const Products = require("../models/Products");*/

// //use course model
// const Course = require("../models/Course")

const bcrypt = require("bcrypt");
const auth = require("../auth");

//user registration
/*module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName:  reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
		
	})

	return newUser.save().then((user, error)=>{
		if(error){
			return false
		}
		else{
			return user;
		}
	})
}*/

module.exports.registerUser = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((existingUser) => {
    if (existingUser) {
      return `Email already exists, try another email.`; // User already exists
    }
    else {
      const newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10),
        mobileNo: reqBody.mobileNo
      });

      return newUser.save().then((savedUser, error) => {
        if (error) {
          return false; // Error occurred while saving user
        } else {
          return savedUser;
        }
      });
    }
  });
};

//retrieve all users
module.exports.getAllUsers =()=>{
	return User.find({}).then(result => {
		return result;
	})
}

//user login / authentication
module.exports.loginUser = (reqBody)=>{
	return User.findOne({email: reqBody.email}).then(result=>{
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,  result.password)
			if(isPasswordCorrect){
				return{access: auth.createAccessToken(result)}
			}
		}
	})
}

//set user as an admin
module.exports.setAdmin =(reqBody)=>{
	return User.findById(reqBody.userId).then(result=>{
		result.isAdmin = true;
		return result.save().then((user, error) => {
			if(error){
				return false
			}
			else{
				return user;
			}
		})
	})
}

//set user admin back to regular user
module.exports.removeAdmin =(reqBody)=>{
	return User.findById(reqBody.userId).then(result=>{
		result.isAdmin = false;
		return result.save().then((user, error) => {
			if(error){
				return false
			}
			else{
				return user;
			}
		})
	})
}

/*//delete a user
module.exports.deleteUser = (userId) => {
  return User.deleteOne({ _id: userId }).then((result) => {
    if (result.deletedCount === 1) {
      return true; // User deleted successfully
    } else {
      return false; // User not found or not deleted
    }
  });
};
*/

//retrive all admin
module.exports.getAllAdmin =()=>{
	return User.find({isAdmin:true}).then(result => {
		return result;
	})
}

//retrieve regular users
module.exports.getAllRegUsers =()=>{
	return User.find({isAdmin:false}).then(result => {
		return result;
	})
}

//check email exists 
/*in the register user controller, it is already checks if a user is already exist by checking the email in the request body. Why? as I observe in registering a new user in google mail, it refuses the gmail that have already exists. skl lang haha so parangbalewala na yung "check if email already exists"*/
/*module.exports.checkEmailExists = (reqBody) =>{

	return User.find({email: reqBody.email}).then(result =>{
		
		if (result.length>0){
			return `Email is already registered.`;
		}
		else{
			return `Email not found.`
		}
	})
};*/


//retrieve user details
module.exports.getProfile = (data)=>{
	
	return User.findById(data.userId)
	 .then(result=>{
		result.password = "";
		return result;
		
		})
}




