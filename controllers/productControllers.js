const Products = require("../models/Products");

const auth = require("../auth");

//add new product
/*module.exports.addProduct = (data) =>{
	let newProduct = new Products ({
		name: data.name,
		description: data.description,
		price: data.price
	})
	return newProduct.save().then((products,error)=>{
		if (error){
			return false
		}
		else{
			return `Product successfully added!`
		}
	})
}*/

module.exports.addProduct = async (data) => {
	try {
		const existingProduct = await Products.findOne({ name: data.name });
		if (existingProduct) {
			return "Product already exists!";
		} else {
			const newProduct = new Products({
				name: data.name,
				description: data.description,
				price: data.price,
			});
			await newProduct.save();
			return data;
		}
	} catch (error) {
		return false;
	}
};

//retrieve active products
module.exports.getAllActiveProducts =()=>{
	return Products.find({isActive:true}).then(result => {
		return result;
	})
}

//retrieve unactive products
module.exports.getAllUnactiveProducts =()=>{
	return Products.find({isActive:false}).then(result => {
		return result;
	})
}


//retrieve all products
module.exports.getAllProducts = () =>{

	return Products.find({}).then(result=>{
		return result;
	});

};

//retriev single order

module.exports.getProduct = (data) =>{
  return Products.findById(data.productId).then(result => {
    return result;
  })
}

//update product infor by admin only
/*module.exports.updateProduct = (reqParams, reqBody) =>{
	if(!reqParams.isAdmin){
		return `User is not authorized to update product info.`
	}
	let updatedProduct = {
		name : reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Products.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error)=>{
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}*/
module.exports.updateProduct = (reqParams, reqBody) =>{
	let updatedProduct = {
		name : reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	//findByIdAndUpdate(document ID, updatesToBeApplied)
	return Products.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error)=>{
		if(error){
			return false
		}
		else{
			return product ;
		}
	})
}

//archive product(Admin only)
module.exports.archiveProduct = (reqParams) => {
  return Products.findById(reqParams.productId)
    .then((product,error) => {
      if (error) {
        return `Product with ID ${req.productId} not found`;
      }
      product.isActive = false;
      return product.save();
    });
};


//activate product(Admin only) make product available
module.exports.activateProduct = (req) => {
  return Products.findById(req.productId)
    .then((product) => {
      if (!product) {
        return `Product with ID ${req.productId} not found`;
      }
      product.isActive = true;
      return product.save();
    });
};



