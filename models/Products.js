 const mongoose = require("mongoose");

 /*name - string,
description - string,
price - number
isActive - boolean
		   default: true,
createdOn - date
			default: new Date()
orders - [
	{
		orderId - string,
		quantity - number

	}
]
*/

const productsSchema = new mongoose.Schema({
	name: {
		type : String,
		required : [true, "Product name is Required!"]
	},
	description : {
		type : String,
		required: [true, "Description is required!"]
	},
	price : {
		type : Number,
		required : [true, "Price is required!"]
	},
	isActive:{
		type : Boolean,
		default: true
	},
	createdOn : {
		type : Date,
		//new date() express instatiates a new "date" that stores to current date adn aslo time whenever a course is created in our database
		default : new Date()
	},
	orders : [
		{
			orderId : {
				type : String/*,
				required : true*/
			},
			quantity : {
				type : Number,
				required : true
			},
		}
	]
})

module.exports = mongoose.model("Products", productsSchema);