const mongoose = require("mongoose");

/*userId - string
totalAmount - number,
purchasedOn - date
		     default: new Date(),
products - [

	{
		productId - string,
		quantity - number
	}

]*/
const ordersSchema = new mongoose.Schema({
	
	userId : {
		type: String
	},
	totalAmount : {
		type: Number,
		required: true,
		default :0
	},
	purchasedOn : {
		type: Date,
		default: new Date()
	},
	isPaid : {
		type:Boolean,
		default: false
	},
	products : [{
		productId : {
			type: String,
			required: true
		},
		quantity : {
			type : Number,
			required : true
		}, 
		subtotal : {
			type : Number/*,
			required : true*/
		}

	}]
})

module.exports = mongoose.model("Orders", ordersSchema);







