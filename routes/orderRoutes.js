const express = require("express");
const router = express.Router();

const auth = require("../auth");

const orderController = require("../controllers/orderControllers")

const Orders = require("../models/Orders")


/*//Non-Admin create order order needs users data
router.post("/createorder", auth.verify, (req, res)=>{
	if(!auth.decode(req.headers.authorization).isAdmin){
		let data = {
			userId: auth.decode(req.headers.authorization).id
		}
		orderController.order(data).then(resultFromController => res.send(resultFromController));
	} 
	else{
		res.send(`Admin not able to make an order.`)
	}
})*/

//retrieve single order thru orderId
router.get("/:orderId", (req, res)=>{

  orderController.getOrder({orderId:req.params.orderId}).then(resultFromController=>res.send(resultFromController));
})
//create order at first product
router.post("/addtocart", auth.verify, (req, res)=>{
	if(!auth.decode(req.headers.authorization).isAdmin){
		let data = {
			productId : req.body.productId,
			quantity : req.body.quantity,
      userId : auth.decode(req.headers.authorization).id 
		}
		orderController.addToCart(data).then(resultFromController => res.send(resultFromController));
	}
	else{
		res.send(`Admin not allowed.`)
	}
})

//add a product to a specific orderId
router.post("/:orderId/addmore", auth.verify, (req, res)=>{
  if(!auth.decode(req.headers.authorization).isAdmin){
    let data = {
      productId : req.body.productId,
      quantity : req.body.quantity
    }
    let orderId = req.params.orderId;
    orderController.addProductToCart(orderId, data).then(resultFromController => res.send(resultFromController));
  }
  else{
    res.send(`Admin not allowed.`)
  }
});

//retrieve authenticated users's orders
router.get("/cart/:userId", async (req, res) => {
  try {
    const userId = req.params.userId
    const orders = await orderController.getOrdersByUserId(userId);
    res.json(orders);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});


//payment status "pending"
router.patch("/paymentstatuspending/:orderId", auth.verify, (req, res)=>{
  if(auth.decode(req.headers.authorization).isAdmin){
    orderController.pendingOrders({ orderId: req.params.orderId }).then(resultFromController => res.send(resultFromController));
  }
  else{
    res.send(`Failed.`)
  }
})

//payment status "paid"
router.patch("/paymentstatuspaid/:orderId", auth.verify, (req, res)=>{
  if(auth.decode(req.headers.authorization).isAdmin){
    orderController.paidOrders({ orderId: req.params.orderId }).then(resultFromController => res.send(resultFromController));
  }
  else{
    res.send(`Failed.`)
  }
})

//retrieve all products
router.get("/",  (req, res)=>{
  orderController.getAllOrders().then(resultFromController=>res.send(resultFromController));
})

//change product quantity in orders
//orderId in params, then find products.objectid
/*router.patch("/changequantity/:orderId", auth.verify, (req, res)=>{
	orderController.changeQuantity({orderId:req.params.orderId}).then(resultFromController => {
		res.send(resultFromController)
	})
})*/
router.put("/:orderId/product/:productId", auth.verify, (req, res) => {
  if (!auth.decode(req.headers.authorization).isAdmin) {
    let data = {
      orderId: req.params.orderId,
      productId: req.params.productId,
      quantity: req.body.quantity,
    };
    orderController
      .updateOrderProduct(data)
      .then((resultFromController) => res.send(resultFromController))
      .catch((error) => res.status(500).send(error));
  } else {
    res.send(`Admin not allowed.`);
  }
});

//retrieve added products
router.get("/addedproducts/:userId", auth.verify, (req, res) =>{
	if(auth.decode(req.headers.authorization).isAdmin){
		res.send('Unauthorized.');

	}
	else{
		orderController.addedProducts(req.params.userId).then(resultFromController=>res.send(resultFromController));
	} 
})

//remove product in the order
/*router.delete("/removeproduct/:orderId", auth.verify, async (req, res) => {
  try {
    const decodedToken = auth.decode(req.headers.authorization);
    const order = await Orders.findById(req.params.orderId);

    if (!order) {
      return res.status(404).json({ success: false, message: "Order not found" });
    }

    if (decodedToken.id !== order.userId) {
      return res.status(401).json({ success: false, message: "Unauthorized" });
    }

    const resultFromController = await orderController.removeOrderProduct(req.params.orderId, req.body.productId);
    return res.json(resultFromController);
  } catch (error) {
    console.log(error);
    return res.status(500).json({ success: false, message: "Internal server error" });
  }
});*/

router.delete("/removeproduct/:orderId", auth.verify, (req,res)=>{
  const decodedToken = auth.decode(req.headers.authorization);
  Orders.findById(req.params.orderId)
    .then(order => {
      if (decodedToken.id !== order.userId) {
        res.send('Unauthorized.');
      } else {
        orderController.removeOrderProduct({ orderId: req.params.orderId, productId: req.body.productId })
          .then(resultFromController => res.send(resultFromController))
          .catch(error => {
            console.log(error);
            res.send({ success: false, message: error.message });
          });
      }
    })
    .catch(error => {
      console.log(error);
      res.send({ success: false, message: error.message });
    });
});

//delete an order
router.delete("/cancelorder/:orderId", auth.verify, (req, res) => {
  const decodedToken = auth.decode(req.headers.authorization);
  Orders.findById(req.params.orderId)
    .then(order => {
      if (decodedToken.id !== order.userId) {
        res.send('Unauthorized.');
      } else {
        Orders.findByIdAndDelete(req.params.orderId)
          .then(() => res.send({ success: true, message: "Order deleted successfully." }))
          .catch(error => {
            console.log(error);
            res.send({ success: false, message: error.message });
          });
      }
    })
    .catch(error => {
      console.log(error);
      res.send({ success: false, message: error.message });
    });
});






module.exports = router;

