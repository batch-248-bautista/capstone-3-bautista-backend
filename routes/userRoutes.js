const express = require("express");
const router = express.Router();

const userController = require ("../controllers/userControllers")

const auth = require("../auth");


//user registration
router.post("/register", (req, res)=>{
	userController.registerUser(req.body).then(
		resultFromController => res.send(resultFromController));
})

//user login / authentication
router.post("/login",(req, res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
})

//set user as admin
/*router.put("/updateAdmin/:userId", auth.verify, (req, res)=>{

	// const userData = auth.decode(req.headers.authorization);

	userController.setAdmin(req.params).then(resultFromController=>res.send(resultFromController));
})*/

//retrieve all products
router.get("/all", auth.verify, (req, res)=>{
	userController.getAllUsers().then(resultFromController=>res.send(resultFromController));
})

router.put("/admin/:userId", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.setAdmin(req.params).then(resultFromController=>res.send(resultFromController))
	}
	else{
		res.send('Unauthorized user.');
	}
})
//set user admin to regular user
router.put("/removeadmin/:userId", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.removeAdmin(req.params).then(resultFromController=>res.send(resultFromController))
	}
	else{
		res.send('Unauthorized user.');
	}
})

//set regular user to admin
router.put("/setadmin/:userId", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.setAdmin(req.params).then(resultFromController=>res.send(resultFromController))
	}
	else{
		res.send('Unauthorized user.');
	}
})

//delete a user by admin
/*router.delete("/delete/:userId", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.deleteUser(req.params).then(resultFromController=>res.send(resultFromController))
	}
	else{
		res.send('Unauthorized user.');
	}
})*/

//retrieve all admin (admin only)
router.get("/admin/all", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.getAllAdmin().then(resultFromController=>res.send(resultFromController));
	}
	else{
		res.send('Unauthorized user.');
	}
})

//retrieve all reg users (admin only)
router.get("/all", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		userController.getAllRegUsers().then(resultFromController=>res.send(resultFromController));
	}
	else{
		res.send('Unauthorized user.');
	}
})
//check email exists
/*router.post("/checkEmailExists", (req, res)=>{
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
})*/


//retrieve user details thru authentication
router.get("/details",auth.verify,(req, res)=>{

	const userData = auth.decode(req.headers.authorization);

	userController.getProfile({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
})




module.exports = router;