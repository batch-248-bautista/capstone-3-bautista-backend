const express = require("express");
const router = express.Router();

const auth = require("../auth");

const productController = require("../controllers/productControllers")

//create product admin only
router.post("/",auth.verify,(req,res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.addProduct(req.body).then(resultFromController=>res.send(resultFromController))
	}
	else{
		res.send('User not allowed to add a product.');
	}
})

//retrieve active products
router.get("/active", (req, res)=>{
	productController.getAllActiveProducts().then(resultFromController=>res.send(resultFromController));
})

//retrieve unactive products
router.get("/unactive", (req, res)=>{
	productController.getAllUnactiveProducts().then(resultFromController=>res.send(resultFromController));
})

//retrieve all products
router.get("/all", (req, res)=>{
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController));
})

//retrieve single product thru productId
router.get("/:productId", (req, res)=>{

	productController.getProduct({productId:req.params.productId}).then(resultFromController=>res.send(resultFromController));
})



// update product information by Admin Only
router.put("/update/:productId", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController=>res.send(resultFromController))
	}
	else{
		res.send('Unauthorized user.');
	}
})

//archive product admin only
router.patch("/archive/:productId", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.archiveProduct({ productId: req.params.productId }).then(resultFromController=>res.send(resultFromController))
	}
	else{
		res.send('Unauthorized user.');
	}
})

//activate product
router.patch("/activate/:productId", auth.verify, (req, res)=>{
	if(auth.decode(req.headers.authorization).isAdmin){
		productController.activateProduct({ productId: req.params.productId }).then(resultFromController=>res.send(resultFromController))
	}
	else{
		res.send('Unauthorized user.');
	}
})


module.exports = router;